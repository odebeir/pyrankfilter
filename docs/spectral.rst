
-----------------------
Spectral filters
-----------------------

The spectral filters implement a sort of bilateral-filtering in the sense that only pixels having a value similar
to the central pixel belong to the neighborhood. It means that borders are much more preserved even for large radius.

The following example shows the difference between a classical median filter and its spectral version, here the tolerance
is set to the interval [-10,10] around the center pixel.

The last image represent the number of pixels actually belonging to the neighborhood (normalized between [0,255]).

.. plot:: code/ex1.py
    :width: 100%


spectral_autolevel
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------- spectral_autolevel -------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
     max=m;
     for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
     min=m;
     if(max-min){
         *ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);
     }
     else{
         *ptemp=(float)*p;
     }


spectral_gradient
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------- spectral_gradient --------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
     max=m;
     for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
     min=m;
     *ptemp=max-min;


spectral_highest
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------- spectral_highest --------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
     max=m;
     if(abs(*p-max)<=delta)*ptemp=MAXI;


spectral_lowest
------------------------------

.. code-block:: c
    :linenos:

    // --------------------------------- spectral_lowest ---------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
     min=m;
     if(abs(*p-min)<=delta)*ptemp=MAXI;


spectral_maximum
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------- spectral_maximum --------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
     *ptemp=m;


spectral_mean
------------------------------

.. code-block:: c
    :linenos:

    // ---------------------------------- spectral_mean ----------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     stot=0;
     sum=0.0f;
     for(m=sga;m<=sgb;m++){
         sum+=(m*histo[m]);
         stot+=histo[m];
     }
     *ptemp=(float)sum/(float)stot;


spectral_median
------------------------------

.. code-block:: c
    :linenos:

    // --------------------------------- spectral_median ---------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     stot=0;
     sum=0;
     for(m=sga;m<=sgb;m++){
         stot+=histo[m];
     }
     for(m=sga;m<=sgb;m++){
         sum+=(float)histo[m];
         if(sum>=(float)stot*.5){
             *ptemp=m;
             m=HISTOSIZE;
         }
     }


spectral_minimum
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------- spectral_minimum --------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
     *ptemp=m;


spectral_morph_contr_enh
------------------------------

.. code-block:: c
    :linenos:

    // ---------------------------- spectral_morph_contr_enh ----------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sgb;(--m>=sga)&&(histo[m]==0););
     max=m;
     for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
     min=m;
     if(abs(*p-min)<abs(*p-max))*ptemp=min;
     else*ptemp=max;


spectral_soft_autolevel
------------------------------

.. code-block:: c
    :linenos:

    // ----------------------------- spectral_soft_autolevel -----------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     for(m=sgb+1;--m&&(histo[m]==0););
     max=m;
     for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
     min=m;
     if(max-min){
         f=((float)(*p-min)*MAXIF)/(float)(max-min);
         f=MAX(0,MIN(MAXIF,f));
         *ptemp=f;
     }
     else{
         *ptemp=(float)*p;
     }


spectral_soft_morph_contr_enh
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------- spectral_soft_morph_contr_enh --------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     stot=0;
     for(m=sga;m<=sgb;m++){
         stot+=histo[m];
     }
     sum=0;
     for(m=sga;m<=sgb;m++){
         sum+=histo[m];
         if(sum>(float)stot*inf){
             min=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     for(m=sga;m<=sgb;m++){
         sum+=histo[m];
         if(sum>(float)stot*sup){
             max=m;
             m=HISTOSIZE;
         }
     }
     if(abs(*p-min)<abs(*p-max))*ptemp=min;
     else*ptemp=max;


spectral_volume
------------------------------

.. code-block:: c
    :linenos:

    // --------------------------------- spectral_volume ---------------------------------
     sga=MAX(0,*p-sa);
     sgb=MIN(MAXIF,*p+sb);
     stot=0;
     for(m=sga;m<=sgb;m++){
         stot+=histo[m];
     }
     *ptemp=255*(float)stot/(float)pop;




