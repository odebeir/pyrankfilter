=====================================
Download
=====================================

* download the distribution file :download:`pyrankfilter-0.3.0.tar.gz <../dist/pyrankfilter-0.3.0.tar.gz>`
* download the distribution file :download:`pyrankfilter-0.2.1.tar.gz <../dist/pyrankfilter-0.2.1.tar.gz>`
* download the distribution file :download:`pyrankfilter-0.2.0.tar.gz <../dist/pyrankfilter-0.2.0.tar.gz>`
* download the distribution file :download:`pyrankfilter-0.1.0.tar.gz <../dist/pyrankfilter-0.1.0.tar.gz>`

* unzip the distribution file in some location

* and run the following command from a terminal at that same location::

    python setup.py install

* now the module should be available in your python environment e.g.

.. code-block:: python

    from pyrankfilter import filter

    print filter.mean.__doc__

