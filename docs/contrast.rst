
----------------------
Contrast enhancement
----------------------

Contrast enhancement is achieved rescaling the local graylevel distribution. The following example shows how maximizing
the local distribution by equalization enhance image contrast.

.. plot:: code/ex3.py
    :width: 100%

autolevel
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------ autolevel ------------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     max=m;
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     min=m;
     if(max-min){
         *ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);
     }
     else{
         *ptemp=(float)*p;
     }


egalise
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- egalise -------------------------------------
     sum=0.0f;
     for(m=0;m<=*p;m++)sum+=histo[m];
     *ptemp=sum*MAXIF/pop;



morph_contr_enh
------------------------------

.. code-block:: c
    :linenos:

    // --------------------------------- morph_contr_enh ---------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     max=m;
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     min=m;
     if(abs(*p-min)<abs(*p-max))*ptemp=min;
     else*ptemp=max;




soft_autolevel
------------------------------

.. code-block:: c
    :linenos:

    // --------------------------------- soft_autolevel ---------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*inf){
             min=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*sup){
             max=m;
             m=HISTOSIZE;
         }
     }
     if(max-min){
         f=((float)(*p-min)*MAXIF)/(float)(max-min);
         f=MAX(0,MIN(MAXIF,f));
         *ptemp=f;
     }
     else{
         *ptemp=(float)*p;
     }

soft_morph_contr_enh
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------ soft_morph_contr_enh ------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*inf){
             min=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*sup){
             max=m;
             m=HISTOSIZE;
         }
     }
     if(abs(*p-min)<abs(*p-max))*ptemp=min;
     else*ptemp=max;
