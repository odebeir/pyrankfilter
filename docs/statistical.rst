
-----------------------
Statistical filters
-----------------------

The statistical filters implement basic statistical measure on the local histogram.

The following example shows how to use ``mean`` and ``mean2`` to build a standard deviation filter.
Be careful, these filters requires the use of ``float_output=True``

.. plot:: code/ex16.py
    :width: 100%

entropy
----------------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- entropy -------------------------------------
     sum=0.0f;
     for(m=0;m<HISTOSIZE;m++){
         if(histo[m]){
             sum-=((float)histo[m]*log((float)histo[m]));
         }
     }
     *ptemp=sum;


mean
----------------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------------- mean --------------------------------------
     sum=0.0f;
     for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
     if(pop){
         *ptemp=sum/pop;
     }


mean2
----------------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------------- mean2 --------------------------------------
     sum=0.0f;
     for(m=0;m<HISTOSIZE;m++)sum+=(m*m*histo[m]);
     if(pop){
         *ptemp=sum/pop;
     }

