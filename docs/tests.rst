===========
Tests
===========
Various test functions are available in the ``pyrankfilter.test`` module.

Kernel tests
------------------------------

.. automodule:: test.test_basic
   :members:
   :undoc-members:


Benchmark
-----------------------------

.. automodule:: test.benchmark
   :members:
   :undoc-members:


