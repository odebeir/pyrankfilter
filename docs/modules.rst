===========
Modules
===========

.. _filter_module:

Filter module
-----------------------------

.. automodule:: pyrankfilter.filter
   :members:
   :undoc-members:


Custom structuring elements module
-----------------------------------------

.. automodule:: pyrankfilter.struct_elem
   :members:
   :undoc-members:

Helpers module
-----------------------------

.. automodule:: pyrankfilter.helpers
   :members:
   :undoc-members:

Rankfilter module
-----------------------------

.. automodule:: pyrankfilter.rankfilter
   :members:
   :undoc-members:

