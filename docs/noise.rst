----------------
Noise filtering
----------------
This section presents basics filters such as Local maximum and Local minimum
(which are gray level version of binary erosion and dilation) and a basic rank filter that returns the value of the
gray level having a certain rank (a fraction of the number of pixels of the neighborhood) in the local gray
level distribution. Median filter is in fact a particular case when rank is equal to .5.

The following example shows how the object are filtered with a median filter with an increasing radius size.

.. plot:: code/ex10.py
    :width: 100%


maximum
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- maximum -------------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     *ptemp=m;


median
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- median -------------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=(float)histo[m];
         if(sum>=pop*.5){
             *ptemp=m;
             m=HISTOSIZE;
         }
     }

minimum
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- minimum -------------------------------------
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     *ptemp=m;


modal
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------------- modal --------------------------------------
     max=0;
     pmax=0;
     for(m=0;m<HISTOSIZE;m++){
         if(max<histo[m]){
             max=histo[m];
             pmax=m;
         }
     }
     *ptemp=pmax;

rank
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------------- rank --------------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=(float)histo[m];
         if(sum>pop*rank){
             *ptemp=m;
             m=HISTOSIZE;
         }
     }


mean
------------------------------
The following two filters are not exactly rank filters, since only the histogram sum is used. Mean filter is a
linear filter. However, since no complex computation was involved, it was added to the available kernels.

.. code-block:: c
    :linenos:

    // -------------------------------------- mean --------------------------------------
     sum=0.0f;
     for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
     if (pop){
        *ptemp=sum/pop;
     }



soft_mean
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------ soft_mean ------------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*inf){
             min=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*sup){
             max=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     count=0;
     for(m=min;m<max;m++){
         sum+=(m*histo[m]);
         count+=histo[m];
     }
     if(pop){
         f=(float)(sum)/(float)count;
     }
     else{
         f=0;
     }
     *ptemp=f;
