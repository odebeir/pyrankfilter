---------
Threshold
---------

Rankfilters can be used as local threshold, e.g. by comparing local mean with the current pixel value.

.. plot:: code/ex2.py
    :width: 100%


median_threshold
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------- median_threshold --------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=(float)histo[m];
         if(sum>pop*0.5){
             if(*p+delta>m)*ptemp=MAXI;
             m=HISTOSIZE;
         }
     }

threshold
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------ threshold ------------------------------------
     sum=0.0f;
     for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
     if(*p+delta>sum/pop)*ptemp=MAXI;






