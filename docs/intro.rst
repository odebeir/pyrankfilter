=============
Introduction
=============

Context description
-----------------------------
Rank filter are used in a variety of ways in image processing, for noise filtering, contrast enhancement,
but also specific point detection or threshold.

The underlying idea is that the pixels belonging to a certain neighborhood are ranked with respect to their gray value.
These filters are typically non-linear filters, since they cannot be obtained by linear operation on the neighborhood.

A local equalization is done on a complete image and a circular neighborhood like the example below:


.. code-block:: python

    from scipy import misc
    from pyrankfilter import filter

    im = misc.imread('../test/data/cameraman.tif')
    f = filter.egalise(im,radius = 30)

Algorithm
-----------------------------
The rank is computed using a moving histogram. New pixels entering the neighborhood are added to the local histogram,
pixels going out being subtracted from the distribution. A each step of the image scan, a process is then applied to
the local histogram. For example, the local maximum consist in returning the highest graylevel having a non zero value
in the histogram.

.. highlight:: c

.. code-block:: c

    // -------------------- maximum --------------------
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    *ptemp=m;

or for the local morphological contrast enhancement, consisting in replacing each pixel by the local maximum OR the
local minimum depending of its closest original value.

.. code-block:: c

    // -------------------- morph_contr_enh --------------------
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if(abs(*p-min)<abs(*p-max))*ptemp=min;
    else*ptemp=max;

The filter functions
---------------------------------

The module ``filter`` contains the essential of the filter function.


A typical usage is given below:

.. code-block:: python

    import numpy as npy
    from pyrankfilter import filter

    im = npy.zeros((100,100),dtype='uint8')
    im[:5,:5] = 255
    f = filter.mean(im,radius = 30,verbose = False)

see :ref:`filter_module` for a complete list of available functions.

The parameters used are :

radius *[optional (see below)]*
    The radius defined the neighborhood used (circular shaped). The following example shows
    a morphological gradient of increasing size.

    .. plot:: code/ex4.py
        :width: 100%

struct_elem *[optional (see below)]*
    The structuring element to be used (a 2D numpy array, element <>0 belong to the element)

struct_elem_center *[optional (see below)]*
    The structuring element center


infSup *[optional]*
    Tolerance to be used for some filters such as soft filters where one c an specify a inferior and a superior rank
    to used instead of using local absolute minimum and maximum.

spectral_interval *[optional]*
    R-The spectral interval (i.e. number of gray levels darker and brighter) to consider a pixel belonging to the pixel
    neighborhood. The default  is (5,5).

mask *[optional]*
    An optional mask parameter, an boolean image having the same size as the input image, defines the image area that
    will be filtered, mask pixels being False are set to 0.

    By default the complete image area is filtered, which means that even for large radii, the image borders are processed.

    The following example illustrates how a boolean mask specifies the processed area.

    .. plot:: code/ex5.py
        :width: 100%

    The next example illustrate the number of pixels belonging the neighborhood taken into account. All the image is filtered
    excepted the rectangular part.

    .. plot:: code/ex15.py
            :width: 100%

verbose *[optional]*
    debug purpose

force *[optional]*
    force the re-compilation of the c-function, if False, the usual behaviour is used i.e. the function is compiled
    during the first call, every successive call of the same kernel will use the compiled code in cache
    (see weave module documentation for more details).

Structuring element
---------------------------------

There is two way to define the structuring element, if only radius is given, the ranfilter function will use a circular
structuring element. If struct_elem is a valid 2D numpy array, it will be used as a custom structuring element, here the
origin can be given by giving struct_elem_center (default is None i.e. the center of the custom element).

The following example illustrate the difference between a circular and un square shaped structuring element.

.. plot:: code/ex14.py
    :width: 100%


Performance
---------------------------------

We compared the ``rankfilter`` function (with the 'rank' kernel) with the ``percentile`` function available in scipy.ndimage.

The obtained results are similar, if we take a radius equal roughly to half the percentiles size.

An other difference is the shape of the structuring element which is rectangular for ``percentile`` and circular for
``rankfilter``.

The results shows that the computation time of ``rankfilter`` is increasing **linearly** with the radius, which is normal
since the local histogram is computed in an incremental way.

.. plot:: code/ex11.py
     :width: 100%