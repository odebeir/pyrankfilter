-----------
Detectors
-----------

Rank filters can be used to test if a pixel with respect to its neighborhood, for example, one can test if the pixel
center is the highest or lowest value of the neighborhood. The following example illustrate the detection of the local
maxima, minima and extrema.

.. plot:: code/ex9.py
    :width: 100%

extrema
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- extrema -------------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     max=m;
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     min=m;
     if((abs(*p-min)<=delta)||(abs(*p-max)<=delta))*ptemp=MAXI;

highest
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- highest -------------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     max=m;
     if(abs(*p-max)<=delta)*ptemp=MAXI;


lowest
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- lowest -------------------------------------
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     min=m;
     if(abs(*p-min)<=delta)*ptemp=MAXI;
