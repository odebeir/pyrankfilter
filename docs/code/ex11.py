# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as np
from time import time

import context
from pyrankfilter import filter
from scipy.ndimage.filters import percentile_filter
import matplotlib.pyplot as plt

im = plt.imread('../../test/data/cameraman.tif')[::2,::2]

#first call the function to compile in cache
fr = filter.rank(im,radius = 2,infSup=[.5,.5])

data = []

for n in range(1,16,2):
    start = time()
    fr = filter.rank(im,radius = n,infSup=[.5,.5])
    timedelta_r = time()-start
    t_r = 'rankfilter ($radius=%.1f$): %f msec'%(n,timedelta_r)

    start = time()
    fp = percentile_filter(im,50,size=n*2-1)
    timedelta_p = time()-start
    t_p = 'percentile ($size=%d$): %f msec'%(n*2-1,timedelta_p)

    data.append((n,timedelta_r,timedelta_p))

data = np.asarray(data)

plt.figure(figsize=[10,7])
plt.subplot(2,2,1)
plt.imshow(fp,origin='upperleft')
plt.xlabel(t_p)
plt.subplot(2,2,2)
plt.imshow(fr,origin='upperleft')
plt.xlabel(t_r)
plt.subplot(2,2,3)
plt.plot(data[:,0],data[:,1:])
plt.legend(['rankfilter','percentile'])
plt.xlabel('filter size')
plt.ylabel('time')

plt.show()
