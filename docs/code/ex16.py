# -*- coding: utf-8 -*-
# compares custom and circular structuring element

import matplotlib.pyplot as plt
import numpy as npy
from pyrankfilter import filter

elem = npy.ones((50,50),dtype='uint8')
im = plt.imread('../../test/data/cameraman.tif')

mean = filter.mean(im,radius=5,float_output=True)
#squared gray level mean
mean2 = filter.mean2(im,radius=5,float_output=True)

s = npy.sqrt(mean2-mean**2)

e = filter.entropy(im,radius=5,float_output=True)

plt.figure(figsize=[10,6])
plt.subplot(3,2,1)
plt.imshow(im,origin='lower')
plt.xlabel('original')
plt.subplot(3,2,2)
plt.imshow(mean,origin='lower')
plt.xlabel('$\mu_g$')
plt.subplot(3,2,3)
plt.imshow(mean2,origin='lower')
plt.xlabel('$1/N\sum(g^2)$')
plt.subplot(3,2,4)
plt.imshow(s,origin='lower')
plt.xlabel('$\sigma_g^2$')
plt.subplot(3,2,5)
plt.imshow(e,origin='lower')
plt.xlabel('entropy')
plt.show()
