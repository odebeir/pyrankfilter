# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import filter


radius = 5
tol = 10
im = plt.imread('../../test/data/cameraman.tif')[200:300,360:480]
bh = filter.bottomhat(im,radius = radius)
gr = filter.gradient(im,radius = radius)
th = filter.tophat(im,radius = radius)
ms = filter.meansubstraction(im,radius = radius)

plt.figure()
plt.subplot(2,2,1)
plt.imshow(bh,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('bottom hat')
plt.subplot(2,2,2)
plt.imshow(th,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('top hat')
plt.subplot(2,2,3)
plt.imshow(gr,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('gradient')
plt.subplot(2,2,4)
plt.imshow(ms,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('mean substraction')

plt.show()
