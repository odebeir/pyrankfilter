# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import filter

radius = 30
im = plt.imread('../../test/data/cameraman.tif')
f = filter.egalise(im,radius = radius,infSup=(.1,.9))

plt.figure(figsize=[10,10])
plt.imshow(f,origin='upperleft',cmap=plt.cm.gray)
plt.title('%s ($r=%d$)'%('egalise',radius))

plt.show()
