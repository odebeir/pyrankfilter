# -*- coding: utf-8 -*-
# compares custom and circular structuring element

import matplotlib.pyplot as plt
import numpy as npy
from pyrankfilter import filter

elem = npy.ones((50,50),dtype='uint8')
im = plt.imread('../../test/data/cameraman.tif')
f = filter.maximum(im,struct_elem = elem,struct_elem_center=None)
g = filter.maximum(im,radius = 25)
plt.figure(figsize=[10,6])
plt.subplot(1,2,1)
plt.imshow(f,origin='lower',vmin=0,vmax=255)
plt.xlabel('custom structuring\n element')
plt.subplot(1,2,2)
plt.imshow(g,origin='lower',vmin=0,vmax=255)
plt.xlabel('circular structuring\n element')
print f.shape
plt.show()
