# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
from numpy import linspace,pi

import context
from pyrankfilter import raster_ellipsis

import matplotlib.pyplot as plt



small_radius = 2
big_radius = 15

for i,theta in enumerate(linspace(0,pi/2,8)):
    plt.subplot(2,4,i+1)
    T = raster_ellipsis(small_radius,big_radius,theta)
    plt.imshow(T,interpolation='nearest',origin='lower',extent = [-T.shape[0]/2.,T.shape[0]/2.,-T.shape[1]/2.,T.shape[1]/2.])
    plt.plot(0,0,'o')

plt.show()
