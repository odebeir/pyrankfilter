# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import filter

radius = 15
im = plt.imread('../../test/data/cameraman.tif')
f = filter.median(im,radius = radius,infSup=(.1,.9))

im[:,im.shape[1]/2:] = f[:,im.shape[1]/2:]

plt.figure(figsize=[10,10])
plt.imshow(im,origin='upperleft',cmap=plt.cm.gray)
plt.title('%s ($r=%d$)'%('median',radius))

plt.show()
