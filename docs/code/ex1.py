# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import filter


radius = 5
tol = 10
im = plt.imread('../../test/data/cameraman.tif')[200:300,360:480]
mean = filter.median(im,radius = radius)
smean = filter.spectral_median(im,radius = radius,spectral_interval=(tol,tol))
svol = filter.spectral_volume(im,radius = radius,spectral_interval=(tol,tol))

plt.figure()
plt.subplot(2,2,1)
plt.imshow(im,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('original')
plt.subplot(2,2,2)
plt.imshow(mean,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('median $r=%d$'%radius)
plt.subplot(2,2,3)
plt.imshow(smean,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('spectral median $r=%d$, $tol=%d$'%(radius,tol))
plt.subplot(2,2,4)
plt.imshow(svol,origin='upperleft',cmap=plt.cm.hot)
plt.xlabel('spectral volume $r=%d$, $tol=%d$'%(radius,tol))

plt.show()
