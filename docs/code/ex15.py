# -*- coding: utf-8 -*-
# compares custom and circular structuring element

import matplotlib.pyplot as plt
import numpy as npy
from pyrankfilter import filter

elem = npy.ones((50,50),dtype='uint8')
im = plt.imread('../../test/data/cameraman.tif')
mask_image = npy.ones_like(im,dtype=bool)
mask_image[200:300,100:250] = False

f = filter.volume(im,radius=25,mask=mask_image,float_output=True)

plt.figure(figsize=[10,6])
plt.subplot(1,2,1)
plt.imshow(mask_image,origin='lower')
plt.xlabel('mask')
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(f,origin='lower')
plt.xlabel('surface of \nthe neighborhood')
plt.colorbar()
plt.show()
