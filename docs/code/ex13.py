# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
from numpy import linspace,pi,asarray
import numpy as npy

import context
from pyrankfilter import filter
from pyrankfilter import raster_ellipsis

import matplotlib.pyplot as plt


im = plt.imread('../../test/data/cameraman.tif')

small_radius = 2
big_radius = 10
E = []
for theta in linspace(0,pi,8):
    E.append(raster_ellipsis(small_radius,big_radius,theta))

for i,element in enumerate(E):
    ax = plt.subplot(2,4,i+1)
    plt.imshow(element,interpolation='nearest',origin='lower')

plt.figure(figsize=[10,10])
for i,element in enumerate(E):
    r = filter.median(im,struct_elem = element)
    plt.subplot(2,4,i+1)
    plt.imshow(r,origin='lower',vmin=0,vmax=255,interpolation='nearest')

plt.show()
