# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as npy

import context
from pyrankfilter import filter

ima = plt.imread('../../test/data/cameraman.tif')[200:320,300:480]

fig = plt.figure(figsize=[10,7])

lo = filter.median(ima,radius = 2, infSup = [10,10])
hi = filter.median(ima,radius = 5, infSup = [10,10])
ext = filter.median(ima,radius = 20, infSup = [10,10])
plt.subplot(2,2,1)
plt.imshow(ima,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('original')
plt.subplot(2,2,2)
plt.imshow(lo,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('median $r=2$')
plt.subplot(2,2,3)
plt.imshow(hi,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('median $r=5$')
plt.subplot(2,2,4)
plt.imshow(ext,origin='upperleft',cmap=plt.cm.gray,vmin=0,vmax=255)
plt.xlabel('median $r=20$')
plt.show()
