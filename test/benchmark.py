# -*- coding: utf-8 -*-
'''
Performance test for different tasks
'''
__author__ = 'Olivier Debeir'


from pyrankfilter import filter
from pyrankfilter.helpers import timeit
from time import sleep,time
from scipy import misc

def benchmark_compilation():
    """Provides a quick and dirty benchmark for computation time evaluation

:param: no param
:returns: nothing

* a first run evaluate compilation+execution time

* a second run evaluate execution time only (compiled code in cache)
    """
    im = misc.imread('../test/data/cameraman.tif')
    print 'with compilation'
    start = time()
    for i in range(10):
        f = filter.rank(im,radius = 3, force=True)
    timedelta = time()-start
    print 'enlapsed time for 10 filters : %d msec (%f msec each)'%(timedelta,timedelta/10.0)

    print 'without compilation (using cached version)'
    start = time()
    for i in range(10):
        f = filter.rank(im,radius = 3, force=False)
    timedelta = time()-start
    print 'enlapsed time for 10 filters : %d msec (%f msec each)'%(timedelta,timedelta/10.0)




def compare_with_scipy():
    """compares speed between ranfilter('rank') and scipy.ndimage.filters.percentile_filter
    to achieve similar size we set radius = n for the rankfilter and  size = 2*n-1 for the percentile filter
    """
    from scipy.ndimage.filters import percentile_filter
    import matplotlib.pyplot as plt
    import numpy as np

    im = misc.imread('./data/cameraman.tif')
    n = 30

    #first call the function to compile in cache
    fr = filter.rank(im,radius = 2,infSup=[.5,.5])

    data = []

    for n in range(1,20,3):
        start = time()
        fr = filter.rank(im,radius = n,infSup=[.5,.5])
        timedelta_r = time()-start
        t_r = 'rankfilter ($radius=%.1f$): %f msec'%(n,timedelta_r)

        start = time()
        fp = percentile_filter(im,50,size=n*2-1)
        timedelta_p = time()-start
        t_p = 'percentile ($size=%d$): %f msec'%(n*2-1,timedelta_p)

        data.append((n,timedelta_r,timedelta_p))

    data = np.asarray(data)

    plt.subplot(2,2,1)
    plt.imshow(fp)
    plt.xlabel(t_p)
    plt.subplot(2,2,2)
    plt.imshow(fr)
    plt.xlabel(t_r)
    plt.subplot(2,2,3)
    plt.plot(data[:,0],data[:,1:])
    plt.legend(['rankfilter','percentile'])

    plt.show()




if __name__ == "__main__":

    benchmark_compilation()
#    compare_with_scipy()

