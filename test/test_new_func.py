# -*- coding: utf-8 -*-
'''
Test te new function definition
'''
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'
__license__ ="""
pyrankfilter is a python module that implements 2D numpy arrays rank filters, the filter core is C-code
compiled on the fly (with an ad-hoc kernel).

Copyright (C) 2012  Olivier Debeir

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from pyrankfilter.rankfilter import rankfilter,kernel_list,kernel
from pyrankfilter.helpers import auto_indent_c
from pyrankfilter import filter
from pyrankfilter.filter_dec import reindent

import numpy as np

def generate_function_def():
    """this function dump automatically flat function definition
    """
    template="""
@rank_decorator
def {name_l}(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
               mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    \"\"\"
    {name} kernel
    ---------------------

    computes {name} on the local neighborhood.
    computes {name} on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
{kernel}
    \"\"\"
"""
    for name,code in kernel.iteritems():

        clean_code = reindent(4,auto_indent_c(code))
        s = template.format(name=name.upper(),name_l=name,kernel=clean_code)
        print s


if __name__ == "__main__":
    generate_function_def()

    print filter.mean
    print filter.mean.__doc__

    ima = np.zeros((10,10),dtype='uint8')
    ima[5,5] = 255

    r = filter.mean(ima,radius=2)
    r = filter.gradient(r,1)

    print r


