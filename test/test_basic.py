# -*- coding: utf-8 -*-
"""Systematic test of all the available filter cores,
test image is in /data subdir,
resulting filtered images are written in /temp subdir
"""
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'

import unittest
from scipy import misc
import os.path as path
from pyrankfilter.rankfilter import rankfilter,rankfiltercore
import numpy as npy

def testAll():
    """Apply all the available rankfilters to a single cameraman.tif image (radius = 20 and default params)

    :param: no param
    :returns: nothing
    """
    im = misc.imread('./data/cameraman.tif')
    print 'test all'
    for fname in rankfiltercore :
        print 'test: %s'%fname
        f = rankfilter(im,filtName = fname,radius = 15,verbose = False,infSup=(.1,.9))
        misc.imsave('./temp/cameraman_filt_%s.tif'%fname,f)

def testAllSpectral():
    """Apply all the available *spectral* rankfilter to a single cameraman.tif image (radius = 20 and default params)

    :param: no param
    :returns: nothing
    """

    im = misc.imread('./data/cameraman.tif')
    print 'test one'
    fnames = [name for name in rankfiltercore if 'spectral' in name]
    for fname in fnames:
        print 'test: %s'%fname
        f = rankfilter(im,filtName = fname,radius = 30,verbose = False,infSup=(.1,.9),spectral_interval=(10,10))
        misc.imsave('./temp/cameraman_filt_%s.tif'%fname,f)

def testMask():
    """Open a 8 bit image, apply the filter on an image part given by a mask
    the mask property allo to limit the application filter on the mask area, the rest of the image
    is not valid(i.e. is set to 0 outside the dilated mask, pixels situated in the radius distance from the mask should not be used)

    :param: no param
    :returns: nothing
    """
    im = misc.imread('./data/cameraman.tif')
    fname = 'soft_autolevel'
    mask_image = npy.zeros_like(im,dtype=bool)
    mask_image[200:300,100:250] = True
    f = rankfilter(im,filtName = fname,radius = 10,verbose = False, mask = mask_image)
    result = im
    result[mask_image] = f[mask_image]
    misc.imsave('./temp/cameramanMask_filt_%s.tif'%fname,result)

def testUint16():
    """Read a 8 bit image, convert it into uint16 and apply the 11 bit version of the rank filter

    :param: no param
    :returns: nothing
    """
    im = misc.imread('./data/cameraman.tif')
    fname = 'mean'
    f = rankfilter(im.astype('uint16'),bitDepth=11,filtName = fname,radius = 3,verbose = False)
    print f.dtype
    print f
    misc.imsave('./temp/cameraman16_filt_%s.tif'%fname,f)

def testBitDepth():
    """Read a 8 bit image, apply rank filter using different bitdepth ! im type must be uint16

    :param: no param
    :returns: nothing
    """
    im = misc.imread('./data/cameraman.tif')
    fname = 'mean'
    f = rankfilter(im.astype('uint16'),bitDepth=5,filtName = fname,radius = 3,verbose = False)
    print f.dtype
    print f
    misc.imsave('./temp/cameramanDepth_filt_%s.tif'%fname,f)




class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""
    pass

if __name__ == '__main__':

    testAll()
    testAllSpectral()
    testBitDepth()
    testMask()

#    unittest.main()
