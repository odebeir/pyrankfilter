# -*- coding: utf-8 -*-
"""
Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>
"""
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.widgets import  RectangleSelector
from scipy import misc
import matplotlib.cm as cm
from pyrankfilter import rankfilter

class EnhRectangleSelector(RectangleSelector):
    """Extend RectangleSelector behaviour by leaving a dashed rectangle at the last selected position
    """
    def __init__(self,ax,onselect,**kwargs):
        RectangleSelector.__init__(self,ax,onselect,**kwargs)
        self.mem_rect = None

    def release(self, event):
        'on button release event'
        if self.eventpress is None or self.ignore(event): return
        # make the box/line invisible again
        self.to_draw.set_visible(False)
        self.canvas.draw()
        # release coordinates, button, ...
        self.eventrelease = event

        if self.spancoords=='data':
            xmin, ymin = self.eventpress.xdata, self.eventpress.ydata
            xmax, ymax = self.eventrelease.xdata, self.eventrelease.ydata
            # calculate dimensions of box or line get values in the right
            # order
        elif self.spancoords=='pixels':
            xmin, ymin = self.eventpress.x, self.eventpress.y
            xmax, ymax = self.eventrelease.x, self.eventrelease.y
        else:
            raise ValueError('spancoords must be "data" or "pixels"')

        if xmin>xmax: xmin, xmax = xmax, xmin
        if ymin>ymax: ymin, ymax = ymax, ymin

        spanx = xmax - xmin
        spany = ymax - ymin
        xproblems = self.minspanx is not None and spanx<self.minspanx
        yproblems = self.minspany is not None and spany<self.minspany
        if (self.drawtype=='box')  and (xproblems or  yproblems):
            """Box to small"""     # check if drawed distance (if it exists) is
            return                 # not to small in neither x nor y-direction
        if (self.drawtype=='line') and (xproblems and yproblems):
            """Line to small"""    # check if drawed distance (if it exists) is
            return                 # not to small in neither x nor y-direction
        self.onselect(self.eventpress, self.eventrelease)
                                              # call desired function
        self.eventpress = None                # reset the variables to their
        self.eventrelease = None              #   inital values

        #draw the last rectangle
        if self.mem_rect is not None:
            self.mem_rect.remove()
        self.mem_rect = plt.Rectangle((xmin,ymin), spanx, spany,edgecolor='red', facecolor='none',label='ROI',linestyle='dashed')
        self.ax.add_patch(self.mem_rect)
        self.update()

        return False




class Main_handler():
    """main Controler between view and data
    """

    def onselect(self,eclick, erelease):
        'eclick and erelease are matplotlib events at press and release'
        print ' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata)
        print ' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata)
        print ' used button   : ', eclick.button
        (xa,xb,ya,yb)=(min(eclick.xdata,erelease.xdata),max(eclick.xdata,erelease.xdata),
                       min(eclick.ydata,erelease.ydata),max(eclick.ydata,erelease.ydata))
        crop = im[int(ya):int(yb),int(xa):int(xb)]
        if self.process is not None:
            self.crop = np.hstack((crop,process(crop)))            
        else:
            self.crop = crop
        self.update_view()

    def update_view(self):

        if self.target_image is not None:
            self.target_image.remove()
        for i in range(len(self.overlays)):
            obj = self.overlays.pop()
            print 'rem',obj
            obj.remove()

        self.target_image = self.ax_target.imshow(self.crop,cmap=cm.gray,interpolation='nearest')
        #add separator overlay
        if self.process is not None:
            line = self.ax_target.plot([self.crop.shape[1]/2.0-.5,self.crop.shape[1]/2.0-.5],[0,self.crop.shape[0]],                                                                                            color='yellow')
            title = self.ax_target.set_title(self.process.__doc__)
            self.overlays.append(line[0])
        #fix axes limits
        self.ax_target.set_xlim((0,self.crop.shape[1]))
        self.ax_target.set_ylim((self.crop.shape[0],0))
        self.ax_target.figure.canvas.draw_idle()

    def __init__(self,im,ax_main,ax_target,process=None):
        self.im = im
        self.ax_main = ax_main
        self.ax_target = ax_target
        self.process = process
        self.ax_main.imshow(self.im, interpolation='nearest',cmap=cm.gray)
        self.rect = None
        self.target_image = None
        self.overlays = []

        rectprops = dict(facecolor='red', edgecolor = 'red',alpha=0.5, fill=True)
        lineprops = dict(color='black', linestyle=':', linewidth = 10, alpha=0.5)
        self.rs = EnhRectangleSelector(self.ax_main, self.onselect, drawtype='box', rectprops=rectprops,
                                       lineprops=lineprops,useblit=False)



class EventHandler:
    def __init__(self,mfig):
        self.fig = mfig.fig
        self.ax = mfig.ax
        self.im = mfig.im
        self.fig.canvas.mpl_connect('button_press_event', self.onpress)

    def onpress(self, event):
        if event.inaxes!=self.ax:
            return
        xi, yi = (int(round(n)) for n in (event.xdata, event.ydata))
        value = self.im[xi,yi]
        print xi,yi,value


if __name__=='__main__':

    #process to apply to the cropped zone
    def process(im):
        """Spectral Mean
        """
        f = rankfilter(im,filtName = 'spectral_mean',radius = 20,verbose = False,infSup=(25,.9),spectral_interval=(20,20))

        return f

    #data
    im = misc.imread('data/cameraman.tif')

    #views
    main_fig = plt.figure()
    ax_main = plt.subplot(1,1,1)
    target_fig = plt.figure()
    ax_target = plt.subplot(1,1,1)

    #controler
    handler = Main_handler(im,ax_main,ax_target,process = process)


    plt.show()

