# -*- coding: utf-8 -*-

import pyrankfilter

import unittest


class AdvancedTestSuite(unittest.TestCase):
    """Advanced test cases."""

    def test_thoughts(self):
        pyrankfilter.test()


if __name__ == '__main__':
    unittest.main()
