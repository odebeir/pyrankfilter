About
================================

This project is a Python module for various 2D image rank filters.

The main routine is compiled on the fly from a generic c-code (for the local histogram computation)
and a specific kernel c-code that evaluate the filter from the local histogram.

The function takes numpy 2D array as input and delivers numpy 2D array.

Bitbucket repository : `Learn more <https://odebeir@bitbucket.org/odebeir/pyrankfilter.git>`_

Author's project website : `<http://homepages.ulb.ac.be/~odebeir/pyrankfilter>`_

Roadmap
=================================

* add filters that returns floats e.g. entropy

* add the possibility to specify the structuring element

* add more examples

* add notebooks examples