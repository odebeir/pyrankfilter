# -*- coding: utf-8 -*-

from distutils.core import setup

from pyrankfilter import __version__

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='pyrankfilter',
    packages=['pyrankfilter'],
    package_data={'pyrankfilter': ['c-code/*.c']},
    version=__version__,
    description='rank filters for 2D numpy array',
    author='Olivier Debeir',
    author_email='odebeir@ulb.ac.be',
    url='https://odebeir@bitbucket.org/odebeir/pyrankfilter.git',
    keywords = ["image processing", "filter", "numpy"],
    classifiers = [
            "Programming Language :: Python",
            "Programming Language :: C",
            "Development Status :: 4 - Beta",
            "Environment :: Other Environment",
            "Intended Audience :: Developers",
            "Intended Audience :: Science/Research",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
            "Operating System :: OS Independent",
            "Topic :: Scientific/Engineering :: Image Recognition",
            "Topic :: Software Development :: Libraries",
            ],

    long_description=readme,
    license=license,

)



