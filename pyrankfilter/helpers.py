# -*- coding: utf-8 -*-
"""
Helper function for the pyrankfilter module
"""
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'
__license__ ="""
pyrankfilter is a python module that implements 2D numpy arrays rank filters, the filter core is C-code
compiled on the fly (with an ad-hoc kernel).

Copyright (C) 2012  Olivier Debeir

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time

def timeit(method):
    """general purpose timing decorator
    """
    def timed(*args, **kw):
        tstart = time.time()
        result = method(*args, **kw)
        tstop = time.time()

        print '%r (%r, %r) %2.2f sec' %(method.__name__, args, kw, tstop-tstart)
        return result

    return timed

import re

def auto_indent_c(s):
    """returns an indented version of string c containing c-code

    example:

    >>> from pyrankfilter.helpers import auto_indent_c
    >>> s = "for(m=HISTOSIZE;(--m)&&(histo[m]==0);); max=m; for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);min=m; if(max-min){*ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);}else{*ptemp=(float)*p;}"
    >>> c = auto_indent_c(s)
    >>> print c
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if(max-min){
        *ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);
    }
    else{
        *ptemp=(float)*p;
    }
    <BLANKLINE>
    <BLANKLINE>

    """
    #sanitize incoming string
    r = s.replace('};','}')
    r = r.replace(' ','')
    r = r.replace('\n','')

    #search for ()
    intervals = []
    plevel = 0
    for pos,c in enumerate(r):
        if c=='(':
            plevel += 1
            if plevel==1:
                a = pos
        if c==')':
            plevel -= 1
            if plevel == 0:
                b = pos
                intervals.append((a,b))

    #add \n after ; (except for those inside (...) )
    t = ''
    p0 = 0
    for m in re.finditer(r";", r):
        t = t + r[p0:m.start()+1]
        p0 = m.end()
        #check if ';' is inside ()
        check = False
        for a,b in intervals:
            if (m.start()>=a) and (m.start()<b):
                check = True
        if not check:
            t = t+'\n'
    t = t + r[p0:]
    #add  \n after { and }
    t = t.replace('{','{\n')
    t = t.replace('}','}\n')

    #indent properly
    ilevel = 0
    out = ''
    for pos,line in enumerate(t.split('\n')):
        if '{' in line:
            out = out+ilevel*'    '+line+'\n'
            ilevel += 1
        elif '}' in line:
            ilevel -= 1
            out = out+ilevel*'    '+line+'\n'
        else:
            out = out+ilevel*'    '+line+'\n'

    return out

def reindent(n,txt):
    r = ''.join(['%s%s'%(' '*n,s) for s in txt.splitlines(True)])
    return r

if __name__ == "__main__":
    #automatic testing with doctest module see >>> lines in the function doc
    print __license__
    import doctest
    doctest.testmod()
