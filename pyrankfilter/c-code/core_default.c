/*
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'
__license__ ="""
pyrankfilter is a python module that implements 2D numpy arrays rank filters, the filter core is C-code
compiled on the fly (with an ad-hoc kernel).

Copyright (C) 2012  Olivier Debeir

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//Process the image using a disk structuring element

#define VERBOSE %(verbose)s
#define DATATYPE %(datatype)s
#define DATATYPE_OUT %(datatype_out)s
#define HISTOSIZE %(histosize)s
#define HISTOSIZE1 %(histosize_1)s
#define MAXI %(maxi)s
#define MEDI %(medi)s
#define MAXIF %(maxif)s
#define MEDIF %(medif)s
#define MAXRADIUS %(maxradius)s
#define _PROCESSING_ %(processing)s

#define MIN(a,b) (a>b?(b):(a))
#define MAX(a,b) (a<b?(b):(a))

DATATYPE *IN = (DATATYPE *) PyArray_GETPTR1(extIn_array,0);
DATATYPE *MASK = (DATATYPE *) PyArray_GETPTR1(extMask_array,0);
DATATYPE_OUT *OUT = (DATATYPE_OUT *) PyArray_GETPTR1(extOut_array,0);
int N_IN = PyArray_SIZE(extIn_array);
int m_IN = PyArray_DIM(extIn_array,0);
int n_IN = PyArray_DIM(extIn_array,1);
int size = radius;
int inumx = n_IN;
int inumy = m_IN;
int min,max,pmax,count;
float f;
float rank = infRange;
float inf = infRange;
float sup = supRange;
float delta = infRange;
int sa = infSpectralRange;
int sb = supSpectralRange;
int sga,sgb,stot;

int histo[HISTOSIZE];
DATATYPE *p0,*p1,*p2,*p3,*p4,*p5,*p;
DATATYPE_OUT *ptemp;
int i,j,k,l,m;
long int t;
float pop,sum;
int size2;
int dcn[MAXRADIUS*2+1];
int dcs[MAXRADIUS*2+1];
int dce[MAXRADIUS*2+1];
int dco[MAXRADIUS*2+1];
int d;

size2=(int)(size*2.0f+1.0f);
for(i=0;i<(int)size+1;i++){
    d=(int)(sqrt((double)(size*size)-(double)(i*i)));
    dco[((int)size)-i]= - d + (((int)size)-i)*inumx;
    dco[((int)size)+i]= - d + (((int)size)+i)*inumx;
    dce[((int)size)-i]= + d + (((int)size)-i)*inumx;
    dce[((int)size)+i]= + d + (((int)size)+i)*inumx;
    dcn[((int)size)-i]= - d*inumx + (((int)size)-i);
    dcn[((int)size)+i]= - d*inumx + (((int)size)+i);
    dcs[((int)size)-i]= + d*inumx + (((int)size)-i);
    dcs[((int)size)+i]= + d*inumx + (((int)size)+i);}
p=IN;
ptemp=OUT;
t=MASK-IN;
for(i=0;i<HISTOSIZE;i++)histo[i]=0;
pop=0.0f;
p0=p+(int)size;
for(j=0;j<size2;j++){
    for(i=0;i<size2;i++){
        d=ceil(sqrt((size-(float)i)*(size-(float)i)+(size-(float)j)*(size-(float)j)));
        if(d<=(int)size){
        if(*(p+t+i+j*inumx)){
        histo[*(p+i+j*inumx)]++;
        pop++;}}}}
p1=p0+1;
p2=p0-1;
p3=p0;
p4=p+(int)size*inumx;
p5=p4+inumx;
p+=((int)size+(int)size*inumx);
ptemp+=((int)size+(int)size*inumx);
j=size;
l=inumx-2*size;
for(;;){
    for(i=l;--i;){
        _PROCESSING_
        for(k=0;k<size2;k++){
        if(*(p1+dce[k]+t)){histo[*(p1+dce[k])]++;pop++;}
        if(*(p0+dco[k]+t)){histo[*(p0+dco[k])]--;pop--;}}
        p++;ptemp++;p0++;p1++;p2++;p3++;p4++;p5++;}
    _PROCESSING_
    j++;
    if(j>=inumy-size) break;
    for(k=0;k<size2;k++){
    if(*(p5+dcs[k]+t)){histo[*(p5+dcs[k])]++;pop++;}
    if(*(p4+dcn[k]+t)){histo[*(p4+dcn[k])]--;pop--;}}
    p+=inumx;ptemp+=inumx;p0+=inumx;p1+=inumx;p2+=inumx;p3+=inumx;p4+=inumx;p5+=inumx;
    for(i=l;--i;){
        _PROCESSING_
        for(k=0;k<size2;k++){
        if(*(p2+dco[k]+t)){histo[*(p2+dco[k])]++;pop++;}
        if(*(p3+dce[k]+t)){histo[*(p3+dce[k])]--;pop--;}}
        p--;ptemp--;p0--;p1--;p2--;p3--;p4--;p5--;}
    _PROCESSING_
    j++;
    if(j>=inumy-size) break;
    for(k=0;k<size2;k++){
    if(*(p5+dcs[k]+t)){histo[*(p5+dcs[k])]++;pop++;}
    if(*(p4+dcn[k]+t)){histo[*(p4+dcn[k])]--;pop--;}}
    p+=inumx;ptemp+=inumx;p0+=inumx;p1+=inumx;p2+=inumx;p3+=inumx;p4+=inumx;p5+=inumx;}
