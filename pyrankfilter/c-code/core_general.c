/*
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'
__license__ ="""
pyrankfilter is a python module that implements 2D numpy arrays rank filters, the filter core is C-code
compiled on the fly (with an ad-hoc kernel).

Copyright (C) 2012  Olivier Debeir

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//Process the image using a custom structuring element
#define VERBOSE %(verbose)s
#define DATATYPE %(datatype)s
#define DATATYPE_OUT %(datatype_out)s
#define HISTOSIZE %(histosize)s
#define HISTOSIZE1 %(histosize_1)s
#define MAXI %(maxi)s
#define MEDI %(medi)s
#define MAXIF %(maxif)s
#define MEDIF %(medif)s
#define MAXEDGE %(maxedge)s
#define _PROCESSING_ %(processing)s

#define MIN(a,b) (a>b?(b):(a))
#define MAX(a,b) (a<b?(b):(a))

DATATYPE *IN = (DATATYPE *) PyArray_GETPTR1(extIn_array,0);
DATATYPE *MASK = (DATATYPE *) PyArray_GETPTR1(extMask_array,0);
DATATYPE_OUT *OUT = (DATATYPE_OUT *) PyArray_GETPTR1(extOut_array,0);
DATATYPE *STRUCT_ELEM = (unsigned char *) PyArray_GETPTR1(struct_elem_array,0);
unsigned char *pstruct;

int N_IN = PyArray_SIZE(extIn_array);
int m_IN = PyArray_DIM(extIn_array,0);
int n_IN = PyArray_DIM(extIn_array,1);

int M_elem = PyArray_DIM(struct_elem_array,0);
int N_elem = PyArray_DIM(struct_elem_array,1);
int m_c = center_m;
int n_c = center_n;

int inumx = n_IN;
int inumy = m_IN;
int min,max,pmax,count;
float f;
float rank = infRange;
float inf = infRange;
float sup = supRange;
float delta = infRange;
int sa = infSpectralRange;
int sb = supSpectralRange;
int sga,sgb,stot;

int histo[HISTOSIZE];
DATATYPE *p0,*p1,*p2,*p3,*p4,*p5,*p;
DATATYPE_OUT *ptemp;
int i,j,k,l,m;
long int t;
float pop,sum;
int dcn[MAXEDGE];
int dcs[MAXEDGE];
int dce[MAXEDGE];
int dco[MAXEDGE];
int n_ns=0;
int n_oe=0;
int d;

//Creates NS attack borders
pstruct = STRUCT_ELEM;
i=0;
for(j=0;j<N_elem;j++){
    if (*(pstruct+i*N_elem+j)){
        dcn[n_ns++] = -n_c + j +(i- m_c) * inumx;
    }
}
for(i=1;i<M_elem;i++){
    for(j=0;j<N_elem;j++){
        if ( *(pstruct+i*N_elem+j) && (! *(pstruct+(i-1)*N_elem+j)) ){
            dcn[n_ns++] = -n_c + j +(i- m_c) * inumx;
        }
    }
}
n_ns=0;
i = M_elem-1;
for(j=0;j<N_elem;j++){
    if (*(pstruct+i*N_elem+j)){
        dcs[n_ns++] = -n_c + j + (i-m_c) * inumx;
    }
}
for(i=0;i<M_elem-1;i++){
    for(j=0;j<N_elem;j++){
        if ( *(pstruct+i*N_elem+j) && (! *(pstruct+(i+1)*N_elem+j)) ){
            dcs[n_ns++] = -n_c + j +(i- m_c) * inumx;
        }
    }
}
//oe
j=0;
for(i=0;i<M_elem;i++){
    if (*(pstruct+i*N_elem+j)){
        dco[n_oe++] = -n_c + j +(i- m_c) * inumx;
    }
}
for(j=1;j<N_elem;j++){
    for(i=0;i<M_elem;i++){
        if ( *(pstruct+i*N_elem+j) && (! *(pstruct+i*N_elem+j-1)) ){
            dco[n_oe++] = -n_c + j +(i- m_c) * inumx;
        }
    }
}
n_oe=0;
j = N_elem-1;
for(i=0;i<M_elem;i++){
    if (*(pstruct+i*N_elem+j)){
        dce[n_oe++] = -n_c + j + (i-m_c) * inumx;
    }
}
for(j=0;j<N_elem-1;j++){
    for(i=0;i<M_elem;i++){
        if ( *(pstruct+i*N_elem+j) && (! *(pstruct+i*N_elem+j+1)) ){
            dce[n_oe++] = -n_c + j +(i- m_c) * inumx;
        }
    }
}

//debug
/*
for(j=0;j<N_elem;j++){ printf("%%d\\n",dcn[j]);}
for(j=0;j<N_elem;j++){ printf("%%d\\n",dcs[j]);}
for(i=0;i<M_elem;i++){ printf("%%d\\n",dco[i]);}
for(i=0;i<M_elem;i++){ printf("%%d\\n",dce[i]);}
*/
//Initialisation of the histogram

p=IN;
ptemp=OUT;
t=MASK-IN;
pstruct = STRUCT_ELEM;

for(i=0;i<HISTOSIZE;i++)histo[i]=0;
pop=0.0f;

for(i=0;i<M_elem;i++){
    for(j=0;j<N_elem;j++){
        if (*(pstruct+i*N_elem+j) && *(p+i*inumx+j)){
            histo[*(p+i*inumx+j)]++;
            pop++;
        }
    }
}

p = IN + (n_c+m_c*inumx);
ptemp = OUT + (n_c+m_c*inumx);

p0=p;
p1=p+1;
p2=p-1;
p3=p;
p4=p;
p5=p+inumx;

j=m_c;
l=inumx-N_elem+2;
for(;;){
    for(i=l;--i;){
        _PROCESSING_
        for(k=0;k<n_oe;k++){
            if(*(p1+dce[k]+t)){histo[*(p1+dce[k])]++;pop++;}
            if(*(p0+dco[k]+t)){histo[*(p0+dco[k])]--;pop--;}
        }
        p++;ptemp++;p0++;p1++;p2++;p3++;p4++;p5++;}
    _PROCESSING_
    j++;
    if(j>=inumy-M_elem+m_c+1) break;
    for(k=0;k<n_ns;k++){
        if(*(p5+dcs[k]+t)){histo[*(p5+dcs[k])]++;pop++;}
        if(*(p4+dcn[k]+t)){histo[*(p4+dcn[k])]--;pop--;}
    }
    p+=inumx;ptemp+=inumx;p0+=inumx;p1+=inumx;p2+=inumx;p3+=inumx;p4+=inumx;p5+=inumx;
    for(i=l;--i;){
        _PROCESSING_
        for(k=0;k<n_oe;k++){
            if(*(p2+dco[k]+t)){histo[*(p2+dco[k])]++;pop++;}
            if(*(p3+dce[k]+t)){histo[*(p3+dce[k])]--;pop--;}
        }
        p--;ptemp--;p0--;p1--;p2--;p3--;p4--;p5--;}
    _PROCESSING_
    j++;
    if(j>=inumy-M_elem+m_c+1) break;
    for(k=0;k<n_ns;k++){
        if(*(p5+dcs[k]+t)){histo[*(p5+dcs[k])]++;pop++;}
        if(*(p4+dcn[k]+t)){histo[*(p4+dcn[k])]--;pop--;}
    }
    p+=inumx;ptemp+=inumx;p0+=inumx;p1+=inumx;p2+=inumx;p3+=inumx;p4+=inumx;p5+=inumx;
}
