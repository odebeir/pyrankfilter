# -*- coding: utf-8 -*-
'''
This module provides a convenient way to call filters function using directly python function
instead of filter 'name'
i.e. one separate function definition for each filter
this file use decorator to do most of the job leaving very little function definition
containing basically help and the C-kernel definition

example 1: with a circular structuring element

    >>> import numpy as npy
    >>> from pyrankfilter import filter
    >>> im = npy.zeros((100,100),dtype='uint8')
    >>> im[0:2,0:2] = 255
    >>> f = filter.mean(im,radius = 3)
    >>> print f[:10,:10]
    [[92 72 60 42 14  0  0  0  0  0]
     [72 56 46 33 11  0  0  0  0  0]
     [60 46 37 18  0  0  0  0  0  0]
     [42 33 18  8  0  0  0  0  0  0]
     [14 11  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]]


    example 2: with a custom structuring element

    >>> im = npy.zeros((100,100),dtype='uint8')
    >>> im[1,1] = 255
    >>> elem = npy.asarray([[0,1,0],[1,1,1],[0,1,0]])
    >>> f = filter.maximum(im,struct_elem = elem)
    >>> print f[:10,:10]
    [[  0 255   0   0   0   0   0   0   0   0]
     [255 255 255   0   0   0   0   0   0   0]
     [  0 255   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]]

    example 3: returning a float value (using custom structuring element)

    >>> im = npy.zeros((100,100),dtype='uint8')
    >>> im[1,1] = 255
    >>> elem = npy.asarray([[0,1,0],[1,1,1],[0,1,0]])
    >>> f = filter.mean(im,struct_elem = elem,float_output=True)
    >>> print f[:5,:5]
    [[   0.   255.     0.     0.     0. ]
     [ 255.   127.5  127.5    0.     0. ]
     [   0.   127.5    0.     0.     0. ]
     [   0.     0.     0.     0.     0. ]
     [   0.     0.     0.     0.     0. ]]

    example 4: returning a float value (using a disk)

    >>> f = filter.mean(im,radius=2,float_output=True)
    >>> print f[:5,:5]
    [[ 42.5         31.875       28.33333397   0.           0.        ]
     [ 31.875       23.18181801  21.25        21.25         0.        ]
     [ 28.33333397  21.25        19.61538506   0.           0.        ]
     [  0.          21.25         0.           0.           0.        ]
     [  0.           0.           0.           0.           0.        ]]
'''
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'
__license__ ="""
pyrankfilter is a python module that implements 2D numpy arrays rank filters, the filter core is C-code
compiled on the fly (with an ad-hoc kernel).

Copyright (C) 2012  Olivier Debeir

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import inspect
from rankfilter import rankfilter_raw
from helpers import reindent

def rank_decorator(method):
    comment,augmented_c_code,c_code = parse_doc(method)
    def local_fun(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                  mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),
                  force = False, float_output = False):

        return rankfilter_raw(ima,c_code,radius=radius,struct_elem=struct_elem,
            struct_elem_center=struct_elem_center,
            infSup=infSup, mask=mask,bitDepth=bitDepth,verbose=verbose,
            spectral_interval=spectral_interval,force=force,float_output=float_output)

    #TODO directly inject kernel instead of using the filter name
    kernel = reindent(4,inspect.cleandoc(augmented_c_code))

    autodoc = """
{doc}

The kernel C-code is as follow :

.. code-block:: c
    :linenos:

{kernel}
    """.format(doc=comment,kernel=kernel)

    local_fun.__doc__ = autodoc
    local_fun.__name__ = method.__name__

    return local_fun



def parse_doc(fun):
    """identify comment and code portion inside a doc string
    modify fun docstring
    return comment and code
    """
    s = fun.__doc__
    code = ''
    comment = ''
    for t in s.splitlines(True):
        if t.strip().startswith('/* ----- KERNEL C-CODE START HERE '):
            comment = code
            code = ''
        else:
            code = code + t

    #print 'the function :',fun.__name__
    #print 'the code :\n', code
    #print 'the comment :\n', comment

    generic_C_comment="""
    /* available data in the kernel :

    pop is the number of local unmasked pixels
    HISTOSIZE is the number of bins (e.g. 256 for 8-but data)
    MAXI is the maximal value of a pixel
    MAXIF same as MAXI but float
    histo[] contains the local histogram
    *p is the central gray level value
    *ptemp the current filtered pixel where result is to be stored
    inf,sup value given in infSup parameter
    sa,sb value of the spectral_interval parameter
    delta is identical to inf
    */"""

    augmented_code = inspect.cleandoc(generic_C_comment)+'\n'+inspect.cleandoc(code)

    kernel = reindent(4,augmented_code)

    autodoc = """
{doc}

.. code-block:: c
    :linenos:

{kernel}
    """.format(doc=inspect.cleandoc(comment),kernel=kernel)

    fun.__doc__ = autodoc
    return comment,augmented_code,code.replace('\n','')


@rank_decorator
def spectral_median(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                    mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_MEDIAN kernel
    ---------------------
    computes SPECTRAL_MEDIAN on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    stot=0;
    sum=0;
    for(m=sga;m<=sgb;m++){
        stot+=histo[m];
    }
    for(m=sga;m<=sgb;m++){
        sum+=(float)histo[m];
        if(sum>=(float)stot*.5){
            *ptemp=m;
            m=HISTOSIZE;
        }
    }


    """


@rank_decorator
def spectral_soft_autolevel(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_SOFT_AUTOLEVEL kernel
    ---------------------
    computes SPECTRAL_SOFT_AUTOLEVEL on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sgb+1;--m&&(histo[m]==0););
    max=m;
    for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
    min=m;
    if(max-min){
        f=((float)(*p-min)*MAXIF)/(float)(max-min);
        f=MAX(0,MIN(MAXIF,f));
        *ptemp=f;
    }
    else{
        *ptemp=(float)*p;
    }


    """


@rank_decorator
def mean2(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
          mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MEAN2 kernel
    ---------------------
    computes MEAN2 on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0.0f;
    for(m=0;m<HISTOSIZE;m++)sum+=(m*m*histo[m]);
    if(pop){
        *ptemp=sum/pop;
    }


    """


@rank_decorator
def spectral_morph_contr_enh(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                             mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_MORPH_CONTR_ENH kernel
    ---------------------
    computes SPECTRAL_MORPH_CONTR_ENH on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sgb;(--m>=sga)&&(histo[m]==0););
    max=m;
    for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
    min=m;
    if(abs(*p-min)<abs(*p-max))*ptemp=min;
    else*ptemp=max;


    """


@rank_decorator
def rank(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
         mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    RANK kernel
    ---------------------
    computes RANK on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=(float)histo[m];
        if(sum>pop*rank){
            *ptemp=m;
            m=HISTOSIZE;
        }
    }


    """


@rank_decorator
def minimum(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MINIMUM kernel
    ---------------------
    computes MINIMUM on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    *ptemp=m;


    """


@rank_decorator
def entropy(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    ENTROPY kernel
    ---------------------
    computes ENTROPY on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0.0f;
    for(m=0;m<HISTOSIZE;m++){
        if(histo[m]){
            sum-=((float)histo[m]*log((float)histo[m]));
        }
    }
    *ptemp=sum;


    """


@rank_decorator
def spectral_highest(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                     mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_HIGHEST kernel
    ---------------------
    computes SPECTRAL_HIGHEST on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
    max=m;
    if(abs(*p-max)<=delta)*ptemp=MAXI;


    """


@rank_decorator
def threshold(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
              mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    THRESHOLD kernel
    ---------------------
    computes THRESHOLD on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0.0f;
    for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
    if(*p+delta>sum/pop)*ptemp=MAXI;


    """


@rank_decorator
def soft_morph_contr_enh(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                         mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SOFT_MORPH_CONTR_ENH kernel
    ---------------------
    computes SOFT_MORPH_CONTR_ENH on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*inf){
            min=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*sup){
            max=m;
            m=HISTOSIZE;
        }
    }
    if(abs(*p-min)<abs(*p-max))*ptemp=min;
    else*ptemp=max;


    """


@rank_decorator
def highest(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    HIGHEST kernel
    ---------------------
    computes HIGHEST on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    if(abs(*p-max)<=delta)*ptemp=MAXI;


    """


@rank_decorator
def spectral_gradient(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                      mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_GRADIENT kernel
    ---------------------
    computes SPECTRAL_GRADIENT on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
    max=m;
    for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
    min=m;
    *ptemp=max-min;


    """


@rank_decorator
def gradient(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
             mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    GRADIENT kernel
    ---------------------
    computes GRADIENT on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    *ptemp=max-min;


    """


@rank_decorator
def spectral_autolevel(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                       mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_AUTOLEVEL kernel
    ---------------------
    computes SPECTRAL_AUTOLEVEL on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
    max=m;
    for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
    min=m;
    if(max-min){
        *ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);
    }
    else{
        *ptemp=(float)*p;
    }


    """


@rank_decorator
def spectral_minimum(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                     mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_MINIMUM kernel
    ---------------------
    computes SPECTRAL_MINIMUM on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
    *ptemp=m;


    """


@rank_decorator
def spectral_lowest(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                    mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_LOWEST kernel
    ---------------------
    computes SPECTRAL_LOWEST on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sga;(m<=sgb)&&(histo[m]==0);m++);
    min=m;
    if(abs(*p-min)<=delta)*ptemp=MAXI;


    """


@rank_decorator
def egalise(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    EGALISE kernel
    ---------------------
    computes EGALISE on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0.0f;
    for(m=0;m<=*p;m++)sum+=histo[m];
    *ptemp=sum*MAXIF/pop;


    """


@rank_decorator
def median_threshold(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                     mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MEDIAN_THRESHOLD kernel
    ---------------------
    computes MEDIAN_THRESHOLD on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=(float)histo[m];
        if(sum>pop*0.5){
            if(*p+delta>m)*ptemp=MAXI;
            m=HISTOSIZE;
        }
    }


    """


@rank_decorator
def spectral_volume(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                    mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_VOLUME kernel
    ---------------------
    computes SPECTRAL_VOLUME on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    stot=0;
    for(m=sga;m<=sgb;m++){
        stot+=histo[m];
    }
    *ptemp=255*(float)stot/(float)pop;


    """


@rank_decorator
def bottomhat(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
              mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    BOTTOMHAT kernel
    ---------------------
    computes BOTTOMHAT on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    *ptemp=*p-m;


    """


@rank_decorator
def lowest(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
           mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    LOWEST kernel
    ---------------------
    computes LOWEST on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if(abs(*p-min)<=delta)*ptemp=MAXI;


    """


@rank_decorator
def soft_autolevel(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                   mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SOFT_AUTOLEVEL kernel
    ---------------------
    computes SOFT_AUTOLEVEL on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*inf){
            min=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*sup){
            max=m;
            m=HISTOSIZE;
        }
    }
    if(max-min){
        f=((float)(*p-min)*MAXIF)/(float)(max-min);
        f=MAX(0,MIN(MAXIF,f));
        *ptemp=f;
    }
    else{
        *ptemp=(float)*p;
    }


    """


@rank_decorator
def autolevel(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
              mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    AUTOLEVEL kernel
    ---------------------
    computes AUTOLEVEL on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if(max-min){
        *ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);
    }
    else{
        *ptemp=(float)*p;
    }


    """


@rank_decorator
def soft_gradient(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                  mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SOFT_GRADIENT kernel
    ---------------------
    computes SOFT_GRADIENT on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*inf){
            min=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*sup){
            max=m;
            m=HISTOSIZE;
        }
    }
    f=(float)(max-min);
    *ptemp=f;


    """


@rank_decorator
def spectral_maximum(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                     mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_MAXIMUM kernel
    ---------------------
    computes SPECTRAL_MAXIMUM on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    for(m=sgb+1;(--m>=sga)&&(histo[m]==0););
    *ptemp=m;


    """


@rank_decorator
def soft_meansubstraction(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                          mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SOFT_MEANSUBSTRACTION kernel
    ---------------------
    computes SOFT_MEANSUBSTRACTION on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*inf){
            min=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*sup){
            max=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    count=0;
    for(m=min;m<max;m++){
        sum+=(m*histo[m]);
        count+=histo[m];
    }
    if(pop){
        f=((float)(*p)-(float)(sum)/(float)count)/2.0f+MEDIF;
    }
    else{
        f=0;
    }
    *ptemp=f;


    """


@rank_decorator
def volume(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
           mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    VOLUME kernel
    ---------------------
    computes VOLUME on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    *ptemp=pop;


    """


@rank_decorator
def spectral_mean(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                  mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_MEAN kernel
    ---------------------
    computes SPECTRAL_MEAN on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    stot=0;
    sum=0.0f;
    for(m=sga;m<=sgb;m++){
        sum+=(m*histo[m]);
        stot+=histo[m];
    }
    *ptemp=(float)sum/(float)stot;


    """


@rank_decorator
def extrema(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    EXTREMA kernel
    ---------------------
    computes EXTREMA on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if((abs(*p-min)<=delta)||(abs(*p-max)<=delta))*ptemp=MAXI;


    """


@rank_decorator
def tophat(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
           mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    TOPHAT kernel
    ---------------------
    computes TOPHAT on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    *ptemp=m-*p;


    """


@rank_decorator
def spectral_soft_morph_contr_enh(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                                  mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SPECTRAL_SOFT_MORPH_CONTR_ENH kernel
    ---------------------
    computes SPECTRAL_SOFT_MORPH_CONTR_ENH on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sga=MAX(0,*p-sa);
    sgb=MIN(MAXIF,*p+sb);
    stot=0;
    for(m=sga;m<=sgb;m++){
        stot+=histo[m];
    }
    sum=0;
    for(m=sga;m<=sgb;m++){
        sum+=histo[m];
        if(sum>(float)stot*inf){
            min=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    for(m=sga;m<=sgb;m++){
        sum+=histo[m];
        if(sum>(float)stot*sup){
            max=m;
            m=HISTOSIZE;
        }
    }
    if(abs(*p-min)<abs(*p-max))*ptemp=min;
    else*ptemp=max;


    """


@rank_decorator
def soft_mean(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
              mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    SOFT_MEAN kernel
    ---------------------
    computes SOFT_MEAN on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*inf){
            min=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=histo[m];
        if(sum>pop*sup){
            max=m;
            m=HISTOSIZE;
        }
    }
    sum=0;
    count=0;
    for(m=min;m<max;m++){
        sum+=(m*histo[m]);
        count+=histo[m];
    }
    if(pop){
        f=(float)(sum)/(float)count;
    }
    else{
        f=0;
    }
    *ptemp=f;


    """


@rank_decorator
def median(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
           mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MEDIAN kernel
    ---------------------
    computes MEDIAN on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0;
    for(m=0;m<HISTOSIZE;m++){
        sum+=(float)histo[m];
        if(sum>=pop*.5){
            *ptemp=m;
            m=HISTOSIZE;
        }
    }


    """


@rank_decorator
def maximum(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MAXIMUM kernel
    ---------------------
    computes MAXIMUM on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    *ptemp=m;


    """


@rank_decorator
def modal(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
          mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MODAL kernel
    ---------------------
    computes MODAL on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    max=0;
    pmax=0;
    for(m=0;m<HISTOSIZE;m++){
        if(max<histo[m]){
            max=histo[m];
            pmax=m;
        }
    }
    *ptemp=pmax;


    """


@rank_decorator
def meansubstraction(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                     mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MEANSUBSTRACTION kernel
    ---------------------
    computes MEANSUBSTRACTION on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0.0f;
    for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
    *ptemp=(((float)*p-sum/pop)/2.0f+MEDIF);


    """


@rank_decorator
def morph_contr_enh(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
                    mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MORPH_CONTR_ENH kernel
    ---------------------
    computes MORPH_CONTR_ENH on the local neighborhood.

    /* ----- KERNEL C-CODE START HERE  -----  */
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if(abs(*p-min)<abs(*p-max))*ptemp=min;
    else*ptemp=max;


    """


@rank_decorator
def mean(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
         mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """
    MEAN kernel
    ---------------------

    computes MEAN on the local neighborhood.

    example 1: with a circular structuring element

    >>> import numpy as np
    >>> from pyrankfilter import filter
    >>> im = np.zeros((100,100),dtype='uint8')
    >>> im[0:2,0:2] = 255
    >>> f = filter.mean(im,radius = 3)
    >>> print f[:10,:10]
    [[92 72 60 42 14  0  0  0  0  0]
     [72 56 46 33 11  0  0  0  0  0]
     [60 46 37 18  0  0  0  0  0  0]
     [42 33 18  8  0  0  0  0  0  0]
     [14 11  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]
     [ 0  0  0  0  0  0  0  0  0  0]]

    example 2: with a custom structuring element

    >>> im = np.zeros((100,100),dtype='uint8')
    >>> im[1,1] = 255
    >>> elem = np.asarray([[0,1,0],[1,1,1],[0,1,0]])
    >>> f = filter.maximum(im,struct_elem = elem)
    >>> print f[:10,:10]
    [[  0 255   0   0   0   0   0   0   0   0]
     [255 255 255   0   0   0   0   0   0   0]
     [  0 255   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]
     [  0   0   0   0   0   0   0   0   0   0]]

    example 3: returning a float value (using custom structuring element)

    >>> im = np.zeros((100,100),dtype='uint8')
    >>> im[1,1] = 255
    >>> elem = np.asarray([[0,1,0],[1,1,1],[0,1,0]])
    >>> f = filter.mean(im,struct_elem = elem,float_output=True)
    >>> print f[:5,:5]
    [[   0.   255.     0.     0.     0. ]
     [ 255.   127.5  127.5    0.     0. ]
     [   0.   127.5    0.     0.     0. ]
     [   0.     0.     0.     0.     0. ]
     [   0.     0.     0.     0.     0. ]]

    example 4: returning a float value (using a disk)

    >>> f = filter.mean(im,radius=2,float_output=True)
    >>> print f[:5,:5]
    [[ 42.5         31.875       28.33333397   0.           0.        ]
     [ 31.875       23.18181801  21.25        21.25         0.        ]
     [ 28.33333397  21.25        19.61538506   0.           0.        ]
     [  0.          21.25         0.           0.           0.        ]
     [  0.           0.           0.           0.           0.        ]]

    /* ----- KERNEL C-CODE START HERE  -----  */
    sum=0.0f;
    for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
    if(pop){
        *ptemp=sum/pop;
    }


    """



if __name__ == '__main__':
    import doctest

    def invert(ima, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
            mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
        """
        MORPH_CONTR_ENH kernel
        ---------------------
        computes MORPH_CONTR_ENH on the local neighborhood.

        /* ----- KERNEL C-CODE START HERE  -----  */
        *ptemp=255-*p;


        """


    test_results = doctest.testmod()
    print test_results

    print 'before parsing'
    print invert.__doc__

    comment,augmented_code,code = parse_doc(invert)

    print 'after parsing'
    print invert.__doc__

    print 'C-code:'
    print code

