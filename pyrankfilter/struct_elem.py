# -*- coding: utf-8 -*-
'''
* serie of function that build structuring element for the rankfilter function (custom mode)*
'''
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'

import numpy as npy
from numpy import sin,cos,pi,linspace,arange,meshgrid

def raster_ellipsis(small_radius,big_radius,theta):
    """builds a structuring element from an ellipse

    example:

    >>> from pyrankfilter import raster_ellipsis
    >>> T = raster_ellipsis(small_radius=2,big_radius=5,theta=1.1)
    >>> print T*1 #(multiplying by 1 to get integer from boolean)
    [[0 0 0 0 0 0 0 1 1 1 0]
     [0 0 0 0 1 1 1 1 1 1 1]
     [0 0 0 1 1 1 1 1 1 1 0]
     [0 0 1 1 1 1 1 1 1 0 0]
     [0 1 1 1 1 1 1 1 0 0 0]
     [1 1 1 1 1 1 1 0 0 0 0]
     [0 1 1 1 0 0 0 0 0 0 0]]
    """

    alpha = linspace(0.,2*pi,100)

    x1 = small_radius * cos(alpha)
    y1 = big_radius * sin(alpha)

    x = x1*cos(theta) - y1 * sin(theta)
    y = x1*sin(theta) + y1 * cos(theta)

    x_range = arange(round(min(x)),round(max(x))+1)
    y_range = arange(round(min(y)),round(max(y))+1)

    X,Y = meshgrid(x_range,y_range)

    def test(x,y):
        """ check if (x,y) is inside (<0) on (=0) of outside (>0) the ellipse
        """
        xt = x
        yt = y
        x1 = xt*cos(-theta) - yt*sin(-theta)
        y1 = xt*sin(-theta) + yt*cos(-theta)
        x1n = x1/small_radius
        y1n = y1/big_radius

        return x1n**2.+y1n**2. - 1

    T =  test(X,Y)

    return T<.25


if __name__ == "__main__":

    import doctest
    doctest.testmod()

    import matplotlib.pyplot as plt



    small_radius = 2
    big_radius = 15

    for i,theta in enumerate(linspace(0,pi/2,8)):
        plt.subplot(2,4,i+1)
        T = raster_ellipsis(small_radius,big_radius,theta)
        plt.imshow(T,interpolation='nearest',origin='lower',extent = [-T.shape[0]/2.,T.shape[0]/2.,-T.shape[1]/2.,T.shape[1]/2.])
        plt.plot(0,0,'o')

    plt.show()


