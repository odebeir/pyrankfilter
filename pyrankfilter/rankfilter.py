# -*- coding: utf-8 -*-
'''
.. note:: This module provides the scaffold for applying C-kernel on complete numpy arrays normally not to be directly used, use decorator proposed in filter.py instead.
'''
__author__ = 'Copyright (C) 2012, Olivier Debeir <odebeir@ulb.ac.be>'
__license__ ="""
pyrankfilter is a python module that implements 2D numpy arrays rank filters, the filter core is C-code
compiled on the fly (with an ad-hoc kernel).

Copyright (C) 2012  Olivier Debeir

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import numpy as npy
from scipy.weave import inline

def _dtype2ctype(array):
    """convert numpy type in C equivalent type
    """
    types = {
        npy.dtype(npy.float64): 'double',
        npy.dtype(npy.float32): 'float',
        npy.dtype(npy.int32): 'int',
        npy.dtype(npy.int16): 'short',
        npy.dtype(npy.uint8): 'unsigned char',
        npy.dtype(npy.uint16): 'unsigned short',
    }
    return types.get(array.dtype)


def rankfilter_raw(ima, c_kernel, radius=None, struct_elem=None, struct_elem_center=None, infSup = (.1,.9),
               mask = None,bitDepth = 8,verbose = False, spectral_interval = (5,5),force = False, float_output = False):
    """compute the rank filtered 2D image on a radius wide circular window
    could eventually be limited on a given mask
    border of the image are filtered too
    the list of the available filters is given using kernelList()

    :param ima: image array
    :type ima: uint8 or uint16 numpy.ndarray
    :param c_kernel: string containing the kernel C-code (single line without newlines)
    :type filtName: string
    :param bitDepth: number of bits used max 16
    :type bitDepth: int[1-16]
    :param radius: radius of the neighborhood used
    :type radius: float [1-100]
    :param struct_elem: 2D image (if None : disk is used)
    :type struct_elem: numpy.ndarray (max size is 100x100)
    :param struct_elem_center: 2D image (non zero values are the element)
    :type struct_elem_center: int tuple(,). If None, the center is center of the structuring element
    :param infSup: inf and sup value used for 'soft' filters e.g. (.01,.99)
    :type infSup: float tuple (,)
    :param mask: mask image (if None : complete image area is filtered)
    :type mask: bool numpy.ndarray
    :param verbose: True : displays details during C code executes (default is False)
    :type verbose: Bool
    :param spectral_interval: if (a,b) the interval [g-a,g+b] is used to limit the neighborhood to similar points (w.r.t. gray level g) this option is only used with 'spectral' kernels (e.g. spectral_mean)
    :type infSup: float tuple (,)
    :param force: force the rankfilter re-compilation. If force is False, cached version, if present) will be used.
    :type force: bool
    :param float_output: False : force the result to be DOUBLE precision float
    :type float_output: Bool

    :returns: filtered image (same size and type as the original image)
    :raises: ValueError

     """

    if struct_elem is None:
        return _rankfilter_disk_raw(ima, c_kernel, radius, infSup=infSup, mask = mask,bitDepth = bitDepth,verbose = verbose,
            spectral_interval = spectral_interval,force = force, float_output = float_output)
    else:
        return _rankfilter_general_raw(ima, c_kernel, struct_elem=struct_elem, struct_elem_center=struct_elem_center,
            infSup = infSup, mask = mask, bitDepth = bitDepth, verbose = verbose, spectral_interval = spectral_interval,
            force = force, float_output = float_output)


def _rankfilter_disk_raw(ima,c_kernel,radius,infSup = (.1,.9), mask = None,bitDepth = 8,verbose = False,
               spectral_interval = (5,5),force = False, float_output = False):
    """compute the rank filtered 2D image on a radius wide circular window (see rankfilter)
    c_kernel is the C-code snippet to be used
     """

    if not isinstance(ima,npy.ndarray):
        raise TypeError('2D numpy.array expected')
    if not ( (ima.dtype == npy.uint8) | (ima.dtype == npy.uint16)):
        raise TypeError('uint8 or uint16 numpy.array expected')
    if not(len(ima.shape) == 2):
        raise TypeError('2D numpy.array expected')

    DATATYPE =  _dtype2ctype(ima)
    if float_output:
        DATATYPE_OUT = 'double'
    else:
        DATATYPE_OUT = DATATYPE
    MAXRADIUS = 100

    #parameters
    if  ((radius > MAXRADIUS) | (radius<1)) :
        raise ValueError('radius must be [0,%d]'%MAXRADIUS)

    (infRange,supRange) = infSup
    (infSpectralRange,supSpectralRange) = spectral_interval

    if ima.dtype == npy.uint8:
        HISTOSIZE = 256
        MAXI = 255
        MEDI = 127
        MAXIF = '255.0f'
        MEDIF = '127.0f'
    else:
        if bitDepth not in range(0,17):
            raise ValueError('bitDepth must be in [0,16] range')
        HISTOSIZE = 2**bitDepth
        MAXI = HISTOSIZE-1
        MEDI = HISTOSIZE/2 - 1
        MAXIF = '%d.0f'%MAXI
        MEDIF = '%d.0f'%MEDI
        ima = npy.minimum(ima,HISTOSIZE-1) #avoid value >10bit

    #extend image border and create mask
    (m,n) = ima.shape
    extIn   = npy.zeros((m+2*radius, n+2*radius),dtype = ima.dtype, order='C')
    extMask = npy.zeros((m+2*radius, n+2*radius),dtype = ima.dtype, order='C')
    if float_output:
        extOut  = npy.zeros((m+2*radius, n+2*radius),dtype = float, order='C')
    else:
        extOut  = npy.zeros((m+2*radius, n+2*radius),dtype = ima.dtype, order='C')
    extIn[radius:m+radius,radius:n+radius] = ima

    if mask is not None:
        if not isinstance(mask,npy.ndarray):
            raise TypeError('2D numpy.array expected for mask')
        if not( ima.shape == mask.shape):
            raise ValueError('ima(%dx%d) and mask(%dx%d) must have same sizes.'%(ima.shape[0],ima.shape[1],mask.shape[0],mask.shape[1]))
        extMask[radius:m+radius,radius:n+radius] = mask
    else: #default mask covers the entire image
        extMask[radius:m+radius,radius:n+radius] = 1

    #activate or de-activate verbose in C code
    if verbose:
        VERBOSE = 'True'
    else:
        VERBOSE = ''

    #code contains the main circular window displacement loop
    #specific rank filter function is injected via the _PROCESSING_ preprocessing directive

    params = {'verbose':VERBOSE,
              'datatype':DATATYPE,
              'datatype_out':DATATYPE_OUT,
              'histosize':str(HISTOSIZE),
              'histosize_1':str(HISTOSIZE),
              'maxi':str(MAXI),
              'medi':str(MEDI),
              'maxif':MAXIF,
              'medif':MEDIF,
              'maxradius':str(MAXRADIUS),
              'processing':c_kernel}

    #read C code from .c file

    try:
        import pkgutil
        c_code = pkgutil.get_data(__name__, 'c-code/core_default.c')
    except ImportError:
        import pkg_resources
        c_code = pkg_resources.resource_string(__name__, 'c-code/core_default.c')

    #inject parameters(#define, etc) into the C source code
    code = c_code%params

    if verbose:
        print 'C code [%s] ="%s"\nprocess = "%s"'%(filtName,code,kernel[filtName])

    # compile C code
    inline(code,
        ['extIn','extMask','extOut','radius','infRange','supRange','infSpectralRange','supSpectralRange'],
        force=force)

    return extOut[radius:m+radius,radius:n+radius]

def _rankfilter_general_raw(ima, c_kernel, struct_elem, struct_elem_center=None,infSup = (.1,.9), mask = None, bitDepth = 8,
                      verbose = False, spectral_interval = (5,5), force = False, float_output = False):
    """compute the rank filtered 2D image on a custom structuring element (see rankfilter)
    """

    if not isinstance(ima,npy.ndarray):
        raise TypeError('2D numpy.array expected')
    if not ( (ima.dtype == npy.uint8) | (ima.dtype == npy.uint16)):
        raise TypeError('uint8 or uint16 numpy.array expected')
    if not(len(ima.shape) == 2):
        raise TypeError('2D numpy.array expected')

    M_elem,N_elem = struct_elem.shape

    if struct_elem_center is None:
        center_m = M_elem/2
        center_n = N_elem/2
    else:
        center_m,center_n = struct_elem_center

    DATATYPE =  _dtype2ctype(ima)

    if float_output:
        DATATYPE_OUT = 'double'
    else:
        DATATYPE_OUT = DATATYPE

    MAXEDGE = 200

    #parameters
    if  (struct_elem.shape[0] > MAXEDGE) | (struct_elem.shape[1] > MAXEDGE) :
        raise ValueError('struct_elem.shape must be < [%d,%d]'%(MAXEDGE,MAXEDGE))

    (infRange,supRange) = infSup
    (infSpectralRange,supSpectralRange) = spectral_interval

    if ima.dtype == npy.uint8:
        HISTOSIZE = 256
        MAXI = 255
        MEDI = 127
        MAXIF = '255.0f'
        MEDIF = '127.0f'
    else:
        if bitDepth not in range(0,17):
            raise ValueError('bitDepth must be in [0,16] range')
        HISTOSIZE = 2**bitDepth
        MAXI = HISTOSIZE-1
        MEDI = HISTOSIZE/2 - 1
        MAXIF = '%d.0f'%MAXI
        MEDIF = '%d.0f'%MEDI
        ima = npy.minimum(ima,HISTOSIZE-1) #avoid value >10bit

    #extend image border and create mask
    (m,n) = ima.shape
    extIn   = npy.zeros((m+M_elem-1, n+N_elem-1),dtype = ima.dtype, order='C')
    extMask = npy.zeros((m+M_elem-1, n+N_elem-1),dtype = ima.dtype, order='C')
    if float_output:
        extOut  = npy.zeros((m+M_elem-1, n+N_elem-1),dtype = float, order='C')
    else:
        extOut  = npy.zeros((m+M_elem-1, n+N_elem-1),dtype = ima.dtype, order='C')
    extIn[center_m:center_m+ima.shape[0],center_n:center_n+ima.shape[1]] = ima

    struct_elem = struct_elem.astype('uint8')

    if mask is not None:
        if not isinstance(mask,npy.ndarray):
            raise TypeError('2D numpy.array expected for mask')
        if not( ima.shape == mask.shape):
            raise ValueError('ima(%dx%d) and mask(%dx%d) must have same sizes.'%(ima.shape[0],ima.shape[1],mask.shape[0],mask.shape[1]))
        extMask[center_m:center_m+ima.shape[0],center_n:center_n+ima.shape[1]] = mask
    else: #default mask covers the entire image
        extMask[center_m:center_m+ima.shape[0],center_n:center_n+ima.shape[1]] = 1

    #activate or de-activate verbose in C code
    if verbose:
        VERBOSE = 'True'
    else:
        VERBOSE = ''

    #code contains the main circular window displacement loop
    #specific rank filter function is injected via the _PROCESSING_ preprocessing directive

    params = {'verbose':VERBOSE,
              'datatype':DATATYPE,
              'datatype_out':DATATYPE_OUT,
              'histosize':str(HISTOSIZE),
              'histosize_1':str(HISTOSIZE),
              'maxi':str(MAXI),
              'medi':str(MEDI),
              'maxif':MAXIF,
              'medif':MEDIF,
              'maxedge':str(MAXEDGE**2),
              'processing':c_kernel}

    #read C code from .c file
    try:
        import pkgutil
        c_code = pkgutil.get_data(__name__, 'c-code/core_general.c')
    except ImportError:
        import pkg_resources
        c_code = pkg_resources.resource_string(__name__, 'c-code/core_general.c')

    #inject parameters(#define, etc) into the C source code
    code = c_code%params

    if verbose:
        print 'C code [%s] ="%s"\nprocess = "%s"'%(filtName,code,kernel[filtName])

    # compile C code
    inline(code,
        ['extIn','extMask','extOut','struct_elem','center_m','center_n','infRange','supRange','infSpectralRange','supSpectralRange'],
        force=force)

    return extOut[center_m:center_m+ima.shape[0],center_n:center_n+ima.shape[1]]




if __name__ == "__main__":
    #automatic testing with doctest module see >>> lines in the function doc
    print __license__
    import doctest
    doctest.testmod()
#    print help(rankfilter)

